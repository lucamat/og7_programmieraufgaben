package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;

public class BunteRechteckGUI extends JFrame {

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenu delete;
	private JMenuItem menuItemNeuesRechteck;
	private JMenuItem menuItemRandomRechteck;
	private JMenuItem menuItemDeleteRechtecke;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BunteRechteckGUI frame = new BunteRechteckGUI();
		frame.run();
	}

	protected void run() {
		while(true) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.revalidate();
			this.repaint();
		}
		
	}

	/**
	 * Create the frame.
	 */
	public BunteRechteckGUI() {
		this.brc = new BunteRechteckeController();		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1200, 1000);
		contentPane = new Zeichenflaeche(this.brc);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		this.menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		this.menu = new JMenu("Hinzuf�gen");
		this.menuBar.add(menu);
		this.menuItemNeuesRechteck = new JMenuItem("Rechteck hinzuf�gen");
		this.menu.add(this.menuItemNeuesRechteck);
		this.menuItemNeuesRechteck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemNeuesRechteck_Clicked();
				
			}
			
		});
		this.menuItemRandomRechteck = new JMenuItem("Random Rechteck hinzuf�gen");
		this.menu.add(this.menuItemRandomRechteck);
		this.menuItemRandomRechteck.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemRandomRechteck_Clicked();
			}
		});
		this.delete = new JMenu("L�schen");
		this.menuBar.add(delete);
		this.menuItemDeleteRechtecke = new JMenuItem("l�sche alle Rechtecke");
		this.delete.add(menuItemDeleteRechtecke);
		this.menuItemDeleteRechtecke.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				menuItemDeleteRechtecke_Clicked();
			}
		});
		setVisible(true);
	}

	protected void menuItemDeleteRechtecke_Clicked() {
		this.brc.reset();
		
	}

	protected void menuItemRandomRechteck_Clicked() {
		this.brc.generiereZufallsRechtecke(1);
		
	}

	protected void menuItemNeuesRechteck_Clicked() {
		this.brc.rechteckHinzufuegen();
	}
}
