package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Eingabemaske extends JFrame {
	
	private JPanel contentPane;
	private JTextField textField_X;
	private JTextField textField_Y;
	private JTextField textField_Breite;
	private JTextField textField_Hoehe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske frame = new Eingabemaske(new BunteRechteckeController());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Eingabemaske(BunteRechteckeController brc) {
		
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 180, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblX = new JLabel("x=");
		lblX.setBounds(10, 22, 21, 14);
		contentPane.add(lblX);
		
		textField_X = new JTextField();
		textField_X.setHorizontalAlignment(SwingConstants.CENTER);
		textField_X.setBounds(60, 19, 75, 20);
		contentPane.add(textField_X);
		textField_X.setColumns(10);
		
		JLabel lblY = new JLabel("y=");
		lblY.setBounds(10, 47, 21, 14);
		contentPane.add(lblY);
		
		textField_Y = new JTextField();
		textField_Y.setHorizontalAlignment(SwingConstants.CENTER);
		textField_Y.setBounds(60, 44, 75, 20);
		contentPane.add(textField_Y);
		textField_Y.setColumns(10);
		
		JLabel lblBreite = new JLabel("Breite =");
		lblBreite.setBounds(10, 78, 46, 14);
		contentPane.add(lblBreite);
		
		textField_Breite = new JTextField();
		textField_Breite.setHorizontalAlignment(SwingConstants.CENTER);
		textField_Breite.setBounds(60, 75, 75, 20);
		contentPane.add(textField_Breite);
		textField_Breite.setColumns(10);
		
		JLabel lblHoehe = new JLabel("Hoehe =");
		lblHoehe.setBounds(10, 103, 46, 14);
		contentPane.add(lblHoehe);
		
		textField_Hoehe = new JTextField();
		textField_Hoehe.setHorizontalAlignment(SwingConstants.CENTER);
		textField_Hoehe.setBounds(60, 100, 75, 20);
		contentPane.add(textField_Hoehe);
		textField_Hoehe.setColumns(10);
		
		JButton btnRechteckHinzfuegen = new JButton("Rechteck hinzfuegen");
		btnRechteckHinzfuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String x = "" + textField_X.getText();
				String y = "" + textField_Y.getText();
				String breite = "" + textField_Breite.getText();
				String hoehe = "" + textField_Hoehe.getText();
				if (x.matches("[0-9]+")) {
					if (y.matches("[0-9]+")) {
						if (breite.matches("[0-9]+")) {
							if (hoehe.matches("[0-9]+")) {
								Rechteck r = new Rechteck(Integer.parseInt(x),Integer.parseInt(y),Integer.parseInt(breite),Integer.parseInt(hoehe));
								brc.add(r);
								textField_X.setText("");
								textField_Y.setText("");
								textField_Breite.setText("");
								textField_Hoehe.setText("");
							} else {
								textField_Hoehe.setText("Fehler");
							}
						} else {
							textField_Breite.setText("Fehler");
						}
					} else {
						textField_Y.setText("Fehler");
					}
				} else {
					textField_X.setText("Fehler");
				}	
			}
		});
		btnRechteckHinzfuegen.setBounds(10, 137, 154, 23);
		contentPane.add(btnRechteckHinzfuegen);
		setVisible(true);
	}
}
