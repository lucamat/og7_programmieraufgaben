package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {
	
	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String password = "";
	
	public void rechteckEintragen(Rechteck r) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url,user,password);
			String sql = "Insert into T_Rechtecke (x,y,breite,hoehe) values " + "(?,?,?,?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List getAlleRechtecke() {
		List rechtecke = new LinkedList();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url,user,password);
			String sql = "Select * From T_Rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breite = rs.getInt("breite");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x,y,breite,hoehe));
			}
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
	
	public void deleteRechtecke() {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url,user,password);
			String sql = "Delete From T_Rechtecke;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
