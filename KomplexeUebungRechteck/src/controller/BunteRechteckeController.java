package controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.MySQLDatabase;
import model.Rechteck;
import view.Eingabemaske;

public class BunteRechteckeController {

	private List<Rechteck> rechtecke;
	private MySQLDatabase sql;
	
	public BunteRechteckeController() {
		super();
		//rechtecke=new LinkedList();
		this.sql = new MySQLDatabase();
		rechtecke =this.sql.getAlleRechtecke();
	}
	
	public List getRechtecke(){
		return rechtecke;
	}

	public void setRechtecke(List<Rechteck> rechtecke) {
		this.rechtecke = rechtecke;
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
		this.sql.rechteckEintragen(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
		this.sql.deleteRechtecke();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public void generiereZufallsRechtecke(int anzahl) {
		for (int i = 0; i < anzahl; i++) {
			Random zahl = new Random();
			int x = zahl.nextInt(1200);
			int y = zahl.nextInt(1000);
			int breite = zahl.nextInt(1200-x);
			int hoehe = zahl.nextInt(1000-y);
			this.add(new Rechteck(x,y,breite,hoehe));
		}
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckHinzufuegen() {
		new Eingabemaske(this);	
	}
}
